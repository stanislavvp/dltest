#include "Scene.h"
#include <iostream>

Scene::Scene() {
	m_widthWindow = 320;
	m_heightWindow = 200;
	m_fullscreen = false;
	
}
//for init with parameters from cmd line
Scene::Scene(int& wWindow, int &hWindow, bool& fullscreen) {
	m_widthWindow = wWindow;
	m_heightWindow = hWindow;
	m_fullscreen = false;
}

void Scene::PreInit(int& width, int& height, bool& fullscreen) {
	width = m_widthWindow;
	height = m_heightWindow;
	fullscreen = m_fullscreen;
}
bool Scene::Init() {
	
	
	setPosition(&m_player, m_widthWindow / 2, m_heightWindow / 2);
	setPosition(&m_cursor, m_widthWindow / 2, m_heightWindow / 2);

	m_fixedTick = 1.0 / 60.0;
	m_gameClock = getTime();
	
	return true;
}

void Scene::Close() {

}



bool Scene::Tick() {	
	m_dTime = getTime() - m_gameClock;
	while (m_dTime >= m_fixedTick) {
		m_dTime -= m_fixedTick;
		m_gameClock += m_fixedTick;

		drawTestBackground();
		m_player.sprite = createSprite("data/avatar.jpg");
		drawSprite(m_player.sprite, m_player.xPosition, m_player.yPosition);
		m_cursor.sprite = createSprite("data/circle.tga");
		drawSprite(m_cursor.sprite, m_cursor.xPosition, m_cursor.yPosition);
		playerMoving(m_up, m_down, m_right, m_left);

	}
	return false;
}


void Scene::onMouseMove(int x, int y, int xrelative, int yrelative) {
	setPosition(&m_cursor, x, y);
	showCursor(false);	
	
}
void Scene::onMouseButtonClick(FRMouseButton button, bool isReleased) {

	switch (button) {
	case FRMouseButton::LEFT: {
		if (isReleased) {
			
		}
		break;
	}
	}
	
	
}
void Scene::onKeyPressed(FRKey k) {
		switch (k) {
		case FRKey::RIGHT: {	
			m_right = true;
			break;
		}

		case FRKey::LEFT: {
			m_left = true;
			break;
		}

		case FRKey::UP: {
			m_up = true;
			break;
		}

		case FRKey::DOWN: {
			m_down = true;
			break;
		}

		}
}
void Scene::onKeyReleased(FRKey k) {
	switch (k) {
		case FRKey::RIGHT: {	
			m_right = false;;
			break;
		}

		case FRKey::LEFT: {
			m_left = false;
			break;
		}

		case FRKey::UP: {
			m_up = false;
			break;
		}

		case FRKey::DOWN: {
			m_down = false;
			break;
		}

		}
	
}

void Scene::setPosition(MySprite* someSprite, const int& xPos, const int& yPos) {
	someSprite->xPosition = xPos;
	someSprite->yPosition = yPos;
}

void Scene::playerMoving(const bool& up, const bool& down, const bool& right, const bool& left) {
	if (up)
		m_player.yPosition -= m_player.velocity;
	else if (down)
		m_player.yPosition += m_player.velocity;
	else if (right)
		m_player.xPosition += m_player.velocity;
	else if (left)
		m_player.xPosition -= m_player.velocity;
}



void Scene::bang() {
	
}

void Scene::setEnemies() {
	
}

float Scene::getTime() {
	float time = (float)getTickCount() / 100;
	return time;
}

Scene::~Scene() {

	delete m_bullet;

};