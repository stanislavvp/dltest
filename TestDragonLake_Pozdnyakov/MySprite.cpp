#include "MySprite.h"
#include <math.h>

void MySprite::setNormal() {
	cursorDistance = sqrt(speedDirection.X * speedDirection.X + speedDirection.Y * speedDirection.Y);
	speedDirection.setXY(speedDirection.X / cursorDistance, speedDirection.Y / cursorDistance);
}

void MySprite::setSpeedDirection(MySprite cursor) {
	if (cursor.xPosition < xPosition)
		cursor.xPosition = -(cursor.xPosition);
	if (cursor.yPosition < yPosition)
		cursor.yPosition = -(cursor.yPosition);
	speedDirection.setXY(cursor.xPosition, cursor.yPosition);
	setNormal();
	
}

void MySprite::move() {
		xPosition += speedDirection.X;
		yPosition += speedDirection.Y;
}