#pragma once

#include "Framework.h"
#include "Vector.h"

class MySprite {
public:

	void move();
	void setNormal();
	void setSpeedDirection(MySprite cursor);

	Sprite* sprite;

	Vector speedDirection;
	float cursorDistance;
	float velocity = 0.2;
	
	float xPosition;
	float yPosition;

	int step = 10;

};