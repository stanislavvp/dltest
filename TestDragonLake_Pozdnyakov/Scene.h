#pragma once


#include "Framework.h"
#include "Vector.h"
#include"MySprite.h"

class Scene : public Framework {
public:
	Scene();
	Scene(int& wWindow, int& hWindow, bool& fullscreen);

	void PreInit(int& width, int& height, bool& fullscreen);
	bool Init();
	void Close();
	bool Tick();
	void onMouseMove(int x, int y, int xrelative, int yrelative);
	void onMouseButtonClick(FRMouseButton button, bool isReleased);
    void onKeyPressed(FRKey k);
    void onKeyReleased(FRKey k);
	void setPosition(MySprite* someSprite, const int& xPos, const int& yPos);
	void playerMoving(const bool& up, const bool& down, const bool& right, const bool& left);
	void setEnemies();
	void bang();
	float getTime();
	
	
private:
	int m_widthWindow;
	int m_heightWindow;
	bool m_fullscreen;
//player moving states
	bool m_up;
	bool m_down;
	bool m_right;
	bool m_left;
// bang states
	bool m_createBullet;
//FPS
	float m_fixedTick;
	float m_gameClock;
	float m_dTime;
//sprites
	MySprite m_player;
	MySprite m_cursor;
	MySprite* m_bullet;


	
	~Scene();
	
};
